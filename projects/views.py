from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Project
from projects.forms import ProjectForm


# Create your views here.
@login_required
def project_list(request):
    project = Project.objects.filter(owner=request.user)
    context = {"projects": project}
    return render(request, "projects/project.html", context)


@login_required
def show_project(request, id):
    project = Project.objects.get(id=id)
    context = {
        "project": project,
    }
    return render(request, "projects/project_detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save()
            project.save()
            return redirect(project_list)
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }

    return render(request, "projects/create_project.html", context)
